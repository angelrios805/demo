FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
ARG DEBIAN_FRONTEND=noninteractive
ARG source=./IbangDemo2.0
WORKDIR /app
EXPOSE 80 443

COPY $source .
ENTRYPOINT ["dotnet", "IbangDemo2.0.dll"]