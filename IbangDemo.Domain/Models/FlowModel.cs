﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IbangDemo.Domain.Models
{
    public class FlowModel
    {
        public bool Status { get; set; }
        public string Answer { get; set; }
        public double Score { get; set; }
        public List<KeyValuePair<string, int>> Context { get; set; }
        public List<KeyValuePair<string, string>> Entities { get; set; }
    }
}
