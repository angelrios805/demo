﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IbangDemo.Domain.Models
{
    public class AudioRequest
    {
        public QueryInput QueryInput { get; set; }
        public string InputAudio { get; set; }

    }
}
