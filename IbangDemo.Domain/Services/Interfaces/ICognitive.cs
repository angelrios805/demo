﻿using IbangDemo.Infrastructure.Models;
using System.Threading.Tasks;

namespace IbangDemo.Domain.Services.Interfaces
{
    public interface ICognitive
    {
        Task DetectedIntent(AdjuntoModel adjunto);
    }
}
