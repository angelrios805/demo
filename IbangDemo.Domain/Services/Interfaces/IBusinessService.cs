﻿using IbangDemo.Infrastructure.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IbangDemo.Domain.Services.Interfaces
{
    public interface IBusinessService
    {
        void SendBusiness(AdjuntoModel adjunto);
        Task<IEnumerable<AdjuntoModel>> GetAll();
    }
}
