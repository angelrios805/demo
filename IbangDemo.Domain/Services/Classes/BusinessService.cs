﻿using IbangDemo.Domain.Services.Interfaces;
using IbangDemo.Infrastructure.Models;
using IbangDemo.Infrastructure.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IbangDemo.Domain.Services.Classes
{
    public class BusinessService : IBusinessService
    {
        private readonly IRepository<AdjuntoModel> _repository;

        public BusinessService(IRepository<AdjuntoModel> repository)
        {
            this._repository = repository;              
        }

        public async  Task<IEnumerable<AdjuntoModel>> GetAll()
        {
            return await _repository.GetAll();
        }

        public void SendBusiness(AdjuntoModel adjunto)
        {
            _repository.Insert(adjunto);
        }
    }
}
