﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Dialogflow.v2;
using Google.Apis.Dialogflow.v2.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using IbangDemo.Domain.Services.Interfaces;
using IbangDemo.Infrastructure.Models;
using IbangDemo.Infrastructure.Repositories.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace IbangDemo.Domain.Services.Classes
{
    public class DialogFlow : ICognitive
    {
        private readonly IRepository<AdjuntoModel> _repository;
        private readonly IConfiguration _configuration;
        private readonly IBlobStorageRepository _blobStorageRepository;

        public DialogFlow(IRepository<AdjuntoModel> repository,
            IConfiguration configuration,
            IBlobStorageRepository blobStorageRepository)
        {
            _repository = repository;
            _configuration = configuration;
            _blobStorageRepository = blobStorageRepository;

        }

        #region Public Methods


        async Task ICognitive.DetectedIntent(AdjuntoModel adjunto)
        {
            GoogleCloudDialogflowV2DetectIntentRequest audioRequest = new GoogleCloudDialogflowV2DetectIntentRequest();
            GoogleCloudDialogflowV2QueryInput query = new GoogleCloudDialogflowV2QueryInput();
            GoogleCloudDialogflowV2InputAudioConfig audioConf = new GoogleCloudDialogflowV2InputAudioConfig();
            audioRequest.InputAudio = adjunto.Audio;
            audioConf.LanguageCode = "es-CO";
            query.AudioConfig = audioConf;
            audioRequest.QueryInput = query;
                    
            string text = "";
            try
            {
                StringContent content = new StringContent(
                       JsonSerializer.Serialize(audioRequest),
                       Encoding.UTF8,
                       "application/json"
                    );

                DialogflowService service = await GetService();
                GoogleCloudDialogflowV2DetectIntentResponse response = service.Projects.Agent.Sessions.DetectIntent(audioRequest, $"projects/agentepruebaespa-ol-obmr/agent/sessions/123").Execute();
                text = response.QueryResult.QueryText;
            }
            catch (Exception ex)
            {
            }
            adjunto.Texto = text;
            adjunto.Fecha = DateTime.Today;
            SendToInfra(adjunto);
        }


        #endregion


        #region Private Methods

        private async Task<DialogflowService> GetService()
        {
            UserCredential credential;

            string Folder = AppDomain.CurrentDomain.BaseDirectory;
            string sPathFolder = Path.Combine(Folder, "Files");

            if (!Directory.Exists(sPathFolder))
            {
                Directory.CreateDirectory(sPathFolder);
            }

            string sPath = Path.Combine(sPathFolder, "Google.Apis.Auth.OAuth2.Responses.TokenResponse-" + "44");
            if (!File.Exists(sPath))
            {
                File.Create(sPath).Close();
            }

            lock (sPath)
            {
                File.WriteAllText(sPath, "{\"access_token\":\"ya29.a0ARrdaM8_29iIcXhNeZMjiGfnGctZ5-NFZcOxAkkbGoUlhBw-A1J7w6e2DQrdfX9lY_6lQR0asGcMVI_x-g5D5_jrlYxOzMBcG7Q-XV8R6rp1ky1Td3Z9gA6XApZOwcYaCwf3s94gRF-rrvTHGryVwqNPBboZ\",\"expires_in\":3595,\"refresh_token\":\"1//05GCi4Pzn6gfKCgYIARAAGAUSNwF-L9IrtnCCvjy8fzwSBJG1-fHz565JFaEb4kA96UN25Z5olxFt8L_MjJTIyB6myrpSu_mdlMw\",\"scope\":\"https://www.googleapis.com/auth/dialogflow https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email openid\",\"token_type\":\"Bearer\",\"id_token\":\"eyJhbGciOiJSUzI1NiIsImtpZCI6IjQ2Mjk0OTE3NGYxZWVkZjRmOWY5NDM0ODc3YmU0ODNiMzI0MTQwZjUiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhenAiOiIxMDU4ODg2MDM0MjI3LXMyNWptZjFrMjFoamU1bjk5cXFyMDB1YWNoaDA5b2lkLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiYXVkIjoiMTA1ODg4NjAzNDIyNy1zMjVqbWYxazIxaGplNW45OXFxcjAwdWFjaGgwOW9pZC5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbSIsInN1YiI6IjEwMjIyNjIwODM5Mzc4MDU0MjM2OCIsImVtYWlsIjoiYW5nZWxyaW9zODA1QGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJhdF9oYXNoIjoibXBaWG4ybHFlYm5fRHJqbHdBei1PUSIsIm5hbWUiOiLDgW5nZWwgUsOtb3MiLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDMuZ29vZ2xldXNlcmNvbnRlbnQuY29tL2EtL0FPaDE0R2d5bHMzcko5VVhNelJxSHBmajZoNi1YNXlpd1pWaGN3c1RoZHVzcVE9czk2LWMiLCJnaXZlbl9uYW1lIjoiw4FuZ2VsIiwiZmFtaWx5X25hbWUiOiJSw61vcyIsImxvY2FsZSI6ImVzIiwiaWF0IjoxNjI4NjA2NDg2LCJleHAiOjE2Mjg2MTAwODZ9.NsUVpvnFntWhGRV9cHg1RslDjUZwxFTJP7e_TD_1HSH3oi3ARVjJpkoY__tqNPogWgXCkOIs2ap3sLx3U7-zFWffntadMTrPOJdqVcTNQ4b0_x0Yoa0P3e5rQwQWwcGpwRhBgCxrpslMIdQBbI4qFcyj-DsNC1E6VjWFuJm92iuGEq_9U--dxk-46QOPYk0lqK0PZyEr6lUJGxijMKMrUaeCjaYYdxWZw6aZcSFw_-yl9p4VC-UJETmVala83QW2IgtYgCYhHRfgkLydeJmYgp9mPPFsTrr6zYOMOiYt90rafknv1rxrnBVeKWJgfOvy4wdHYwdxgUe4kOqGuTakZw\"}");
            }
            string FileName = "ibang-platform-credentials.json";
            sPath = Path.Combine(Folder, "Files", FileName);

            using (FileStream stream = new FileStream(sPath, FileMode.Open, FileAccess.Read))
            {
                Folder = AppDomain.CurrentDomain.BaseDirectory;
                sPath = Path.Combine(Folder, "Files");

                // Secreto OAuth
                ClientSecrets secrets = GoogleClientSecrets.Load(stream).Secrets;
                string[] scope = new[] { DialogflowService.Scope.Dialogflow };
                FileDataStore dataStore = new FileDataStore(sPath, true);

                credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
                    secrets,
                    scope,
                    "44",
                    CancellationToken.None,
                    dataStore);
            }

            return new DialogflowService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = string.Empty,
            });
        }


        private void SendToInfra(AdjuntoModel adjunto)
        {
            try
            {
                IConfigurationSection blobs = _configuration.GetSection("Blobs");
                string Container = blobs.GetValue<string>("Container");
                string ConnectionString = blobs.GetValue<string>("ConnectionString");

                if (!string.IsNullOrEmpty(adjunto.Audio))
                {
                    FileContract fileContract = new FileContract()
                    {

                        Path = adjunto.Audio,
                        Name = string.Format("{0}", Guid.NewGuid()),
                        NeedResize = false,
                        IsSetBytes = true

                    };

                    fileContract = _blobStorageRepository.ProcessFileAsync(fileContract, Container, ConnectionString).Result;
                    FileContract file = fileContract;

                    if (!(file is null))
                    {
                        adjunto.Audio = file.BlobUrl;
                    }
                }

                if (!string.IsNullOrEmpty(adjunto.Documento))
                {
                    FileContract fileContract = new FileContract()
                    {

                        Path = adjunto.Documento,
                        Name = string.Format("{0}", Guid.NewGuid()),
                        NeedResize = false,
                        IsSetBytes = true,
                        Type = "application/pdf"

                    };

                    fileContract = _blobStorageRepository.ProcessFileAsync(fileContract, Container, ConnectionString).Result;
                    FileContract file = fileContract;

                    if (!(file is null))
                    {
                        adjunto.Documento = file.BlobUrl;
                    }
                }

            }
            catch (Exception ex)
            {

            }
            

            AdjuntoModel respose = _repository.Insert(adjunto);

        }

        #endregion
    }
}
