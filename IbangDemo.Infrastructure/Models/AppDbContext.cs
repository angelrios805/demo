﻿using Microsoft.EntityFrameworkCore;

namespace IbangDemo.Infrastructure.Models
{
    public class AppDbContext : DbContext
    {
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<AdjuntoModel>().Property(x => x.Id).HasDefaultValueSql("NEWID()");            

        }
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }
        
        public DbSet<AdjuntoModel> Adjuntos { get; set; }       
    }
}
