﻿namespace IbangDemo.Infrastructure.Models
{
    public class FileContract
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public string BlobUrl { get; set; }
        public string Type { get; set; }
        public double Mb { get; set; }
        public short Pages { get; set; }
        public int RotationAngle { get; set; }
        public bool NeedResize { get; set; }
        public bool HeaderImg { get; set; }
        public bool IsSetBytes { get; set; }
    }
}
