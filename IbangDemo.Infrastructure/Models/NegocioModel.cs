﻿using System.Collections.Generic;

namespace IbangDemo.Infrastructure.Models
{
    public class NegocioModel
    {
        public int Id { get; set; }
        public string Proveedor { get; set; }
        public List<AdjuntoModel> Adjuntos { get; set; }
    }
}
