﻿using System;

namespace IbangDemo.Infrastructure.Models
{
    public class AdjuntoModel
    {
        public string Id { get; set; }
        public DateTime Fecha { get; set; }
        public string Audio { get; set; }
        public string Documento { get; set; }
        public string Texto { get; set; }
    }
}
