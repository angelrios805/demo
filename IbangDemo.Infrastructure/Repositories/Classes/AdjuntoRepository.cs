﻿using IbangDemo.Infrastructure.Models;
using IbangDemo.Infrastructure.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IbangDemo.Infrastructure.Repositories.Classes
{
    public class AdjuntoRepository : IRepository<AdjuntoModel>
    {
        private readonly AppDbContext context;

        public AdjuntoRepository(AppDbContext context)
        {
            this.context = context;

        }
        public async Task<IEnumerable<AdjuntoModel>> GetAll()
        {
            try
            {
                return await context.Adjuntos.ToListAsync();
            
            }
            catch(Exception ex)
            {

            }
            return null;
        }

        public AdjuntoModel Insert(AdjuntoModel model)
        {

            EntityEntry<AdjuntoModel> modelSaved = context.Add(model);
            context.SaveChangesAsync();
            return modelSaved.Entity;

        }
    }
}
