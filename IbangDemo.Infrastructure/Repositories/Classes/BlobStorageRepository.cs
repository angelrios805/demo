﻿using IbangDemo.Infrastructure.Models;
using IbangDemo.Infrastructure.Repositories.Interfaces;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace IbangDemo.Infrastructure.Repositories.Classes
{
    public class BlobStorageRepository : IBlobStorageRepository
    {
        #region Attributes

        private readonly List<string> Exts;
        private readonly List<string> TypesFiles;
        private const string PdfType = "application/pdf";

        #endregion
      

        public BlobStorageRepository()
        {
            Exts = new List<string>()
            {
                "jpe","jpeg","jpg"
            };

            TypesFiles = new List<string>()
            {
                "image/png", // png
                "image/jpeg", // jpg, jpeg
                "image/svg+xml", // svg
                "image/gif", // gif
                PdfType, // pdf
                "application/msword", // doc
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document", // docx
                "application/vnd.ms-excel", // xls
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // xlsx
                "application/vnd.ms-powerpoint", // ppt
                "application/vnd.openxmlformats-officedocument.presentationml.presentation", // pptx
                "text/plain", // txt
                "audio/ogg; codecs=opus", // audio/.oga whatsapp
                "video/mp4",
                "audio/ogg"// audio twilio
            };
        }

        async Task<FileContract> IBlobStorageRepository.ProcessFileAsync(FileContract file, string containerName, string storageConnectionString)
        {
            if (!CloudStorageAccount.TryParse(storageConnectionString, out CloudStorageAccount storageAccount))
            {
                return file;
            }

            try
            {
                // Create the CloudBlobClient that represents the Blob storage endpoint for the storage account.
                CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();
                storageAccount = null;

                CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference(containerName);
                cloudBlobClient = null;

                if (await cloudBlobContainer.CreateIfNotExistsAsync())
                {
                    // Set the permissions so the blobs are public. 
                    BlobContainerPermissions permissions = new BlobContainerPermissions
                    {
                        PublicAccess = BlobContainerPublicAccessType.Blob
                    };
                    await cloudBlobContainer.SetPermissionsAsync(permissions);
                }

                string nameFile = file.Name.Trim();
                CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(nameFile);
                cloudBlobContainer = null;

                string ext = string.Empty;
                byte[] bytes;

                if (await cloudBlockBlob.ExistsAsync())
                {
                    return file;
                }
                if (!file.IsSetBytes)
                {
                    bytes = await GetBytes(file.Path);
                }
                else
                {
                    bytes = Convert.FromBase64String(file.Path);
                }

                if (bytes.Length == 0)
                {
                    return file;
                }

                if (file.Type == PdfType)
                {
                    SautinSoft.PdfFocus f = new SautinSoft.PdfFocus();
                    f.OpenPdf(bytes);
                    short pageCount = Convert.ToInt16(f.PageCount);
                    f = null;
                    file.Pages = pageCount;
                }
                else
                {
                    file.Pages = 0;
                }

                cloudBlockBlob.Properties.ContentType = file.Type;
                await cloudBlockBlob.UploadFromByteArrayAsync(bytes, 0, bytes.Length);

                file.BlobUrl = cloudBlockBlob.Uri.AbsoluteUri;
                file.Mb = ((double)bytes.Length) / 1024 / 1024;
                cloudBlockBlob = null;

                return file;
            }
            catch (StorageException Ex)
            {
                throw (Ex);
            }
        }



        private async Task<byte[]> GetBytes(string Uri)
        {
            try
            {
                HttpClient client = new HttpClient
                {
                    Timeout = TimeSpan.FromMinutes(3)
                };
                HttpResponseMessage response = await client.GetAsync(Uri);
                client.Dispose();
                client = null;

                IEnumerable<string> contexts = response.Content.Headers.FirstOrDefault(x => string.Equals(x.Key, "Content-Type", StringComparison.OrdinalIgnoreCase)).Value;

                if ((!response.IsSuccessStatusCode) || contexts is null)
                {
                    return new byte[0];
                }

                if (!contexts.Any(x => TypesFiles.Any(y => y == x)))
                {
                    return new byte[0];
                }

                contexts = null;

                byte[] bFile = await response.Content.ReadAsByteArrayAsync();

                return bFile;
            }
            catch (TaskCanceledException TEx)
            {
                if (TEx.CancellationToken.IsCancellationRequested)
                {
                    return new byte[0];
                }
                else
                {
                    throw TEx;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }



    }
}
