﻿using IbangDemo.Infrastructure.Models;
using System.Threading.Tasks;

namespace IbangDemo.Infrastructure.Repositories.Interfaces
{
    public interface IBlobStorageRepository
    {
        Task<FileContract> ProcessFileAsync(FileContract file, string containerName, string storageConnectionString);
    }
}
