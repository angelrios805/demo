﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IbangDemo.Infrastructure.Repositories.Interfaces
{
    public interface IRepository<TModel> where TModel: class, new()
    {
        Task<IEnumerable<TModel>> GetAll();        
        TModel Insert(TModel model);
    }
}
