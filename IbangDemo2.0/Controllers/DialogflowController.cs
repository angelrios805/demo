﻿using Google.Apis.Dialogflow.v2.Data;
using Google.Cloud.Dialogflow.V2;
using Google.Protobuf;
using IbangDemo.Domain.Models;
using IbangDemo.Domain.Services.Interfaces;
using IbangDemo.Infrastructure.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Threading.Tasks;


namespace IbangDemo2._0.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class DialogflowController : ControllerBase
    {
        private static readonly JsonParser jsonParser =
        new JsonParser(JsonParser.Settings.Default.WithIgnoreUnknownFields(true));

        private ICognitive _cognitive;
        private IBusinessService _businessService;
        public DialogflowController(ICognitive cognitive, IBusinessService businessService)
        {
            _cognitive = cognitive;
            _businessService = businessService;
        }

        [HttpPost]
        [Route("GetAction")]
        public async Task DialogAction()
        {            
            string requestJson;
            using (TextReader reader = new StreamReader(Request.Body))
            {
                requestJson = await reader.ReadToEndAsync();
            }
            AdjuntoModel adjuntoModel = Newtonsoft.Json.JsonConvert.DeserializeObject<AdjuntoModel>(requestJson);

            _ = _cognitive.DetectedIntent(adjuntoModel);                    
            
        }


        [HttpPost]
        [Route("SendBusiness")]
        public async Task SendBusiness()
        {            
            string requestJson;
            using (TextReader reader = new StreamReader(Request.Body))
            {
                requestJson = await reader.ReadToEndAsync();
            }

            AdjuntoModel request = Newtonsoft.Json.JsonConvert.DeserializeObject<AdjuntoModel>(requestJson);
            
        }
    }
}
