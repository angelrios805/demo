﻿using IbangDemo.Infrastructure.Models;
using IbangDemo.Infrastructure.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IbangDemo2._0.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AttachedController : ControllerBase
    {
        private readonly IRepository<AdjuntoModel> repository;
        public AttachedController(IRepository<AdjuntoModel> repository)
        {
            this.repository = repository;

        }
        [HttpGet]
        [Route("GetAll")]
        public async Task<IEnumerable<AdjuntoModel>> GetAll()
        {
            return await repository.GetAll();

        }
    }
}
