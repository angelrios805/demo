import { QueryInput } from "./queryInput.model";
export class AudioRequest{
    public queryInput: QueryInput;
    public inputAudio: string;
}