import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AdjuntoReuest } from '../../models/adjunto.model';
import { AudioRequest } from '../../models/audioRequest.model';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(
    private http: HttpClient
  ) { }

  public GetText(audioRequest: AdjuntoReuest): Observable<any> {
    
    return this.http.post('https://localhost:5001/api/Dialogflow/GetAction',JSON.stringify(audioRequest));
  }

  public GetAllAttached(): Observable<any> {
    
    return this.http.get('https://localhost:5001/api/Attached/GetAll');
  }



}
