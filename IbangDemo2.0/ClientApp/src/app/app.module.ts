import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { RegisterComponent } from './pages/register/register.component';
import { InitialComponent } from './pages/initial/initial.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule, MatInputModule, MatPaginatorModule, MatTableModule } from '@angular/material';
import { RecordTableComponent } from './pages/record-table/record-table.component';
import { RegisterGeneralInfoComponent } from './pages/register-general-info/register-general-info.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    FetchDataComponent,
    RegisterComponent,
    InitialComponent,
    RecordTableComponent,
    RegisterGeneralInfoComponent,        
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'counter', component: CounterComponent },
      { path: 'fetch-data', component: FetchDataComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'consult', component: RecordTableComponent },
      { path: 'general', component: RegisterGeneralInfoComponent },
    ]),
    BrowserAnimationsModule,
    MatDialogModule,
    MatTableModule,
    MatPaginatorModule,
    MatInputModule
     
    
  ],
  providers: [
          
  ],
  bootstrap: [AppComponent],
  exports:[
    MatTableModule
  ]
  

})
export class AppModule { }
