import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-initial',
  templateUrl: './initial.component.html',
  styleUrls: ['./initial.component.css']
})
export class InitialComponent implements OnInit {

  constructor(private router: Router,
    ) { }

  ngOnInit() {
  }

  redirectTo(route: string) {
   this.router.navigate([`${route}`]);
  }

}
