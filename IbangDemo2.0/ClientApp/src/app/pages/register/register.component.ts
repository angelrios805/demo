import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { MediaRecorder } from 'extendable-media-recorder';
import { AdjuntoReuest } from 'src/app/shared/models/adjunto.model';
import { AudioRequest } from 'src/app/shared/models/audioRequest.model';
import { RegisterService } from 'src/app/shared/services/register/register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {


  public isRecording: boolean = false;
  public mediaRecorder = null;
  public chunks = [];
  public audioFiles;
  private idInputFile: string = 'InputFile';
  private idModal: string = 'ModalSucces'
  public fileName: string = 'Adjuntar documentos';
  public audioRequest: AudioRequest;
  public audioInBase64: string='';
  public pdfInBase64: string= '';  
  public adjuntoRequest: AdjuntoReuest;  
  public cantItEnd: boolean = false;

  constructor(private cd: ChangeDetectorRef, private dom: DomSanitizer,
    private _register: RegisterService,
    private router: Router,) { }

  ngOnInit() {
    navigator.getUserMedia(
      { audio: true },
      stream => {
        this.mediaRecorder = new MediaRecorder(stream);
        this.mediaRecorder.onstop = e => {

          var blob = new Blob(this.chunks, { type: 'audio/ogg; codecs=opus' });
          this.ConverToBase64(blob, (base64Result) => {            
            this.audioInBase64 = base64Result;
          });

          this.chunks = [];
          var audioURL = URL.createObjectURL(blob);
          this.audioFiles = [];
          this.audioFiles.push(this.dom.bypassSecurityTrustUrl(audioURL));
          console.log('recorder stopped: ', this.audioFiles);
          this.cd.detectChanges();
        };
        this.mediaRecorder.ondataavailable = e => {
          this.chunks.push(e.data);
        };
      },
      () => {
        alert('Error capturing audio.');
      },
    );
  }
  private ConverToBase64 = (blob: any, onFinish: (result: any) => void) => {
    var reader = new FileReader();
    reader.readAsDataURL(blob);
    reader.onloadend = () => {
      var base64data = reader.result;
      onFinish(base64data);
    };
  };

  private SendToService(base64data: string, pdfbase64: string): void {
    let BASE64_MARKER = ';base64,';
    let base64IndexAudio = base64data.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
    let base64Audio = base64data.substring(base64IndexAudio);
    let base64IndexPdf = pdfbase64.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
    let base64AudioPdf = pdfbase64.substring(base64IndexPdf);
    this.adjuntoRequest = {
      Audio: base64Audio,
      Documento: base64AudioPdf,
      Texto: ''
    }
    this._register.GetText(this.adjuntoRequest).subscribe((x: any) => {
      console.log(x);
    }
    );
  }


  public startRecording(): void {
    this.isRecording = !this.isRecording;
    this.mediaRecorder.start();
    console.log(this.mediaRecorder.state);
    console.log('recorder started');
  }

  public stopRecording(): void {
    this.isRecording = !this.isRecording;
    this.mediaRecorder.stop();
    console.log(this.mediaRecorder);
    console.log(this.chunks);
  }

  public Adjutar(): void {
    document.getElementById(this.idInputFile).click();
  }

  public ChangeFile(element: HTMLInputElement): void {
    if (element.value) {
      this.fileName = element.value;
      this.Upload();
    } else {
      this.fileName = 'Adjuntar documentos';
    }
  }

  public redirectTo(route: string): void {
    this.router.navigate([`${route}`]);
  }

  public Finish(): void {        
    this.SendToService(this.audioInBase64, this.pdfInBase64);
    document.getElementById(this.idModal).click();
    this.clear()
  }
  private clear(): void {
    this.audioFiles = [];
    this.fileName = 'Adjuntar documentos';
  }
  private Upload(): void {
    const file: File = (<HTMLInputElement>document.getElementById(this.idInputFile)).files[0];
    this.ConverToBase64(file, (base64Result)=>{
      this.pdfInBase64 = base64Result;      
    });   
  }
  
}