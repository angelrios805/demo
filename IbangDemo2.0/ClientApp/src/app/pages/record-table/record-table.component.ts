import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import axios from 'axios';
import { AdjuntoReuest } from 'src/app/shared/models/adjunto.model';
import { Register } from 'src/app/shared/models/register.model';
import { RegisterService } from 'src/app/shared/services/register/register.service';



@Component({
  selector: 'app-record-table',
  templateUrl: './record-table.component.html',
  styleUrls: ['./record-table.component.css']
})
export class RecordTableComponent implements OnInit {


  constructor(private dom: DomSanitizer,
    private _register: RegisterService,
    private router: Router) {

  }



  public dataSource: MatTableDataSource<AdjuntoReuest>;
  public displayedColumns: string[];

  public datos: AdjuntoReuest[];


  GetData(datos: any) {
    this.displayedColumns = ['fecha', 'audio', 'texto', 'documento'];
    this.dataSource = new MatTableDataSource<AdjuntoReuest>(datos);
    console.log(this.dataSource)

  }

  GetAllAtached() {
    this._register.GetAllAttached().subscribe((x: any) => {

      this.GetData(x);
    });

  }
  OpenPdf(doc: any) {

    window.open(doc, "_self");

  }

  redirectTo(route: string): void {
    this.router.navigate([`${route}`]);
  }

  public ApplyFilter(filterValue: string): void {
    this.dataSource.filter = filterValue;
  }

  ngOnInit() {
    this.GetAllAtached();

  }
}
