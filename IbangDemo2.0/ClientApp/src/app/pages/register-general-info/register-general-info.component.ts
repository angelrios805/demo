import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-general-info',
  templateUrl: './register-general-info.component.html',
  styleUrls: ['./register-general-info.component.css']
})
export class RegisterGeneralInfoComponent implements OnInit {

  constructor(private router: Router,) { }

  public idModal: string = 'ModalSucces'

  public redirectTo(route: string): void {
    this.router.navigate([`${route}`]);
  }

  public Finish(): void {
    document.getElementById(this.idModal).click();    
  }

  ngOnInit() {
  }

}
